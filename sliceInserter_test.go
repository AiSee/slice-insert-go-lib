package sliceInserter

import (
	"fmt"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	i := New(func(chan interface{}) {}, 1, time.Second)
	if i == nil {
		t.Error("new failed")
	}
}

func TestInserter_Add(t *testing.T) {
	type myRow struct{ field1, field2 string }
	var insertCounter int

	i := New(func(rows chan interface{}) {
		for row := range rows {
			if _, ok := row.(myRow); !ok {
				t.Error("interface conversion failed")
				return
			} else {
				insertCounter++
			}
		}
	}, 2, time.Millisecond)

	i.Add(myRow{"Because", "we add 3 rows"})
	i.Add(myRow{"And the limit is", "two"})
	ok := i.Add(myRow{"This row", "will be discarded"})

	i.Wait()
	if insertCounter != 2 {
		t.Errorf("Wrong inserted rows count. Expected: 2, got: %d", insertCounter)
	}
	if ok {
		t.Errorf("No overflow")
	}
}

func TestRaces(t *testing.T) {
	var insertCounter, okCounter int

	i := New(func(rows chan interface{}) {
		for row := range rows {
			if row.(string) != "ok" {
				t.Error("interface conversion failed")
				return
			} else {
				insertCounter++
			}
		}
		return
	}, 1000, 0*time.Second)
	for x := 0; x < 100000; x++ {
		if i.Add("ok") {
			okCounter++
		}
	}
	i.Wait()
	if insertCounter != okCounter {
		t.Error("Wrong inserted rows count", insertCounter)
	}
}

func BenchmarkInsert(b *testing.B) {
	b.StopTimer()

	type myRow struct{ field1, field2 string }

	i := New(func(rows chan interface{}) {
		for row := range rows {
			_ = row.(*myRow)
		}
	}, 10000, time.Second)
	row := myRow{}

	b.StartTimer()
	for x := 0; x < b.N; x++ {
		i.Add(&row)
	}
}

func ExampleInserter() {
	type myRow struct{ field1, field2 string }

	i := New(func(rows chan interface{}) {
		// Здесь нужно описать вставку элементов
		for row := range rows {
			if mr, ok := row.(myRow); !ok {
				fmt.Println("Error: Interface conversion failed!")
				return
			} else {
				fmt.Println(mr.field1 + ", " + mr.field2)
			}
		}
	},
		// Максимальный размер списка сообщений, которые будут храниться перед вставкой. Если собщений накопится больше,
		// новые будут отброшены. Следует выбрать разумное число учитывая сколько объектов может свободно
		// поместиться в оперативке
		1000000,
		// Частота вызова flush(), т.е. непосредственного обращения к базе для сохранения данных
		time.Second)
	i.Add(myRow{"Row 1, Field 1", "Row 1, Field 2"})
	i.Add(myRow{"Row 2, Field 1", "Row 2, Field 2"})
	i.Wait() // Стоит вызывать только перед завершением работы приложения
}
