package sliceInserter

import (
	"time"
)

type Inserter struct {
	queue chan interface{}
	flush func(chan interface{})
}

func (i *Inserter) Add(row interface{}) bool {
	select {
	case i.queue <- row:
		// message sent
		return true
	default:
		// message dropped
		return false
	}
}

// Эта функция может использоваться для ожидания вставки всех строк
// Например, когда приложение завершает работу и Add() больше не вызывается
func (i *Inserter) Wait() {
	for len(i.queue) > 1 {
		time.Sleep(time.Millisecond)
	}
}

func New(flush func(chan interface{}), queueLen int, flushPeriod time.Duration) *Inserter {
	i := Inserter{
		queue: make(chan interface{}, queueLen),
		flush: flush,
	}

	go func() {
		for {
			time.Sleep(flushPeriod)
			i.flush(i.queue)
		}
	}()

	return &i
}
